import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner myscan = new Scanner(System.in);
        System.out.println("Пойти ли мне погулять?...");
        System.out.println("А какая температура на улице?");
        int temp = myscan.nextInt ();
        System.out.println("А что с ветром?");
        int wind = myscan.nextInt();
        System.out.println("А дождь идёт?");
        String israin = myscan.next();
        
        if (israin.toLowerCase().equals("да")) 
        {
            System.out.println("Не, лучше не ходи. А то заболеещь ещё)");
        }
        else if (temp > 30)
        {
            System.out.println("Можешь сходить, но возьми с собой воды!");
        }
        else if (temp < -14)
        {
            System.out.println("Холодновато как-то. Лучше не ходи");
        }
        else if (wind > 15)
        {
            System.out.println("Слишком ветренно, смотри, чтобы не сдуло");
        }
        else if (temp < -5)
        {
            System.out.println("Иди, только оденься потеплее");
        }
        else if (temp > 10 && wind < 5)
        {
            System.out.println("Отличная погода, чтобы прогуляться!!!");
        }
    }
}
